@tag
Feature: Buscar en eBay
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Buscar zapatilla Puma
    Given Acceder a la pagina de Ebay
    And Buscar zapato <producto>
    And Seleccionar Marca PUMA
    And Seleccionar Talla
    And Imprimir el número de resultados
    And Ordenar por precio ascendente
    And Hacer valer la orden tomando los primeros 5 resultados
    And Ordenar por nombre ascendente
    And Ordenar por precio e imprimir en consola

	Examples: 
      |producto|
      |zapato|
