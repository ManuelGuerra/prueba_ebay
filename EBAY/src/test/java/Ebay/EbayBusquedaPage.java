package Ebay;

import java.util.Arrays;
import java.util.Collections;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Definicion.SetUp;

public class EbayBusquedaPage extends PageFactory {

	private WebDriverWait wait;
	//WebDriver driver;
	
	public EbayBusquedaPage(WebDriver driver){
		//this.driver=ldriver;
		//PageFactory.initElements(driver, this);
		//	PageFactory.initElements(driver, this);
		PageFactory.initElements(SetUp.driver, this);
	}
	
	public void EscribirProducto(String producto) {
		SetUp.driver.findElement(By.id("gh-ac")).sendKeys(producto);
		SetUp.driver.findElement(By.id("gh-btn")).click();
	}
	
	public void SeleccionarMarca() throws InterruptedException {
		Thread.sleep(2000);
		SetUp.driver.findElement(By.xpath("//*[@id=\"w3-w0-w2-w2-w6\"]/a/span[1]")).click();
	}
	
	public void SeleccionarTalla() throws InterruptedException {
		Thread.sleep(2000);
		SetUp.driver.findElement(By.xpath("//*[@id=\"w3-w0-w2-multiselect[4]\"]/a/span[1]")).click();
	}
	
	public void ImprimirResultado() {
		String resultado = SetUp.driver.findElement(By.xpath("//*[@id=\"w4\"]/div[2]/div/div[1]/h1")).getText();
		System.out.println("Resultado de búsqueda: "+resultado);
		System.out.println(" ");
	}
	
	public void Ordenar_Ascendente() throws InterruptedException {
		Thread.sleep(2000);
		SetUp.driver.findElement(By.xpath("//*[@id=\"w4-w3\"]/button/div/div")).click();
		SetUp.driver.findElement(By.xpath("//*[@id=\"w4-w3\"]/button/div/div")).click();
		Thread.sleep(1000);
		SetUp.driver.findElement(By.xpath("//*[@id=\"w4-w3\"]/div/div/ul/li[4]/a/span")).click();
	}
	
	public void Validar_E_Imprimir_5_Primeros_Productos() {
		System.out.println("Take the first 5 products with their prices and print them in console.");
		System.out.println("----------------------------------------------------------------------");
		String producto1 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing1\"]/div/div[2]/a/h3")).getText();
		System.out.println("Producto 1: "+producto1);
		System.out.println("");
		String producto2 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing2\"]/div/div[2]/a/h3")).getText();
		System.out.println("Producto 2: "+producto2);
		System.out.println("");
		String producto3 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing3\"]/div/div[2]/a/h3")).getText();
		System.out.println("Producto 3: "+producto3);
		System.out.println("");
		String producto4 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing4\"]/div/div[2]/a/h3")).getText();
		System.out.println("Producto 4: "+producto4);
		System.out.println("");
		String producto5 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing5\"]/div/div[2]/a/h3")).getText();
		System.out.println("Producto 5: "+producto5);
	}
	
	public void Ordenar_x_Nombre_e_Imprimir_Productos() {
		System.out.println("");
		System.out.println("Order and print the products by name (ascendant)");
		System.out.println("-------------------------------------------------");
		String producto1 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing1\"]/div/div[2]/a/h3")).getText();
		String producto2 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing2\"]/div/div[2]/a/h3")).getText();
		String producto3 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing3\"]/div/div[2]/a/h3")).getText();
		String producto4 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing4\"]/div/div[2]/a/h3")).getText();
		String producto5 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing5\"]/div/div[2]/a/h3")).getText();

		String producto[] = {producto1,producto2,producto3,producto4,producto5};
		
		Arrays.sort(producto);
		
		for(String s:producto)
			System.out.println(s);
	}
	
	public void Ordenar_x_Precio_Descendente_e_Imprimir_Productos() {
		System.out.println("");
		System.out.println("Order and print the products by price in descendant mode");
		System.out.println("--------------------------------------------------------");
		String producton1 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing1\"]/div/div[2]/a/h3")).getText();
		String producton2 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing2\"]/div/div[2]/a/h3")).getText();
		String producton3 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing3\"]/div/div[2]/a/h3")).getText();
		String producton4 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing4\"]/div/div[2]/a/h3")).getText();
		String producton5 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing5\"]/div/div[2]/a/h3")).getText();

		
		String producto1 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing1\"]/div/div[2]/div[3]/div[1]/span")).getText().substring(4,SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing1\"]/div/div[2]/div[3]/div[1]/span")).getText().length());
		String producto2 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing2\"]/div/div[2]/div[3]/div[1]/span")).getText().substring(4,SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing2\"]/div/div[2]/div[3]/div[1]/span")).getText().length());
		String producto3 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing3\"]/div/div[2]/div[3]/div[1]/span")).getText().substring(4,SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing3\"]/div/div[2]/div[3]/div[1]/span")).getText().length());
		String producto4 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing4\"]/div/div[2]/div[3]/div[1]/span")).getText().substring(4,SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing4\"]/div/div[2]/div[3]/div[1]/span")).getText().length());
		String producto5 = SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing5\"]/div/div[2]/div[3]/div[1]/span")).getText().substring(4,SetUp.driver.findElement(By.xpath("//*[@id=\"srp-river-results-listing5\"]/div/div[2]/div[3]/div[1]/span")).getText().length());
	
		Double a = Double.parseDouble(producto1);
		Double b = Double.parseDouble(producto2);
		Double c = Double.parseDouble(producto3);
		Double d = Double.parseDouble(producto4);
		Double e = Double.parseDouble(producto5);
		
		Double producto[] = {a,b,c,d,e};
		String producton[] = {producton1,producton2,producton3,producton4,producton5};
		
		Arrays.sort(producto,Collections.reverseOrder());
		
		for(Double s:producto)
			if(s == a) {
			System.out.println(producton1);
			}else if (s == b) {
				System.out.println(producton2);
			}else if (s == c) {
				System.out.println(producton3);
			}else if (s == d) {
				System.out.println(producton4);
			}else {
				System.out.println(producton5);
			}
		
	}
	
}
