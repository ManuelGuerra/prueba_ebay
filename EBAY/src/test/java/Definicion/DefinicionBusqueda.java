package Definicion;

import Ebay.EbayBusquedaPage;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class DefinicionBusqueda{

	EbayBusquedaPage EbayBusquedaPage;
	
	public DefinicionBusqueda() {
		EbayBusquedaPage=new EbayBusquedaPage(SetUp.driver);
	}
	
	@Given("^Acceder a la pagina de Ebay$")
	public void OpenBrowser() throws Throwable {
		SetUp.setUp();
	}
	
	@And("^Buscar zapato([^\"]*)$")
	public void Buscar_Producto(String producto) {
		EbayBusquedaPage.EscribirProducto(producto);
	}
	
	@And("^Seleccionar Marca PUMA$")
	public void Seleccionar_Marca() throws InterruptedException {
		EbayBusquedaPage.SeleccionarMarca();
	}
	
	@And("^Seleccionar Talla$")
	public void Seleccionar_Talla() throws InterruptedException {
		EbayBusquedaPage.SeleccionarTalla();
	}
	
	@And("^Imprimir el número de resultados$")
	public void Escribir_Telefono() {
		EbayBusquedaPage.ImprimirResultado();
	}
	
	@And("^Ordenar por precio ascendente$")
	public void Orden_Ascendente() throws InterruptedException {
		EbayBusquedaPage.Ordenar_Ascendente();
	}
	
	@And("^Hacer valer la orden tomando los primeros 5 resultados$")
	public void Imprimir5procutos() {
		EbayBusquedaPage.Validar_E_Imprimir_5_Primeros_Productos();
	}
	
	@And("^Ordenar por nombre ascendente$")
	public void Ordenar_x_Nombre_e_ImprimirProductos() {
		EbayBusquedaPage.Ordenar_x_Nombre_e_Imprimir_Productos();
	}
	
	@And("^Ordenar por precio e imprimir en consola$")
	public void OrdenarXPrecioEImprimirProductos() {
		EbayBusquedaPage.Ordenar_x_Precio_Descendente_e_Imprimir_Productos();
	}
}
