package Definicion;

import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class SetUp {
	public static WebDriver driver;
	
	@Before
	public static void setUp() throws MalformedURLException{
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Manuel\\git\\repository\\EBAY\\Resource\\chromedriver.exe");
	driver=new ChromeDriver();
	driver.get("https://www.ebay.com");
	driver.manage().window().maximize();
	//((JavascriptExecutor)driver).executeScript("scroll(0,450)");
	}
	
	@After
	public static void tearDown() {
		driver.quit();
	}
	
}
