package PageObjects;

import org.junit.runner.RunWith;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;

@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(jsonReport = "target/cucumber-report/cucumber.json",
retryCount = 3,
detailedReport = true,
detailedAggregatedReport = true,
overviewReport = true,
//coverageReport = true,
jsonUsageReport = "target/cucumber-report/cucumber-usage.json",
usageReport = true,
toPDF = true,
excludeCoverageTags = {"@flaky" },
includeCoverageTags = {"@passed" },
outputFolder = "target/cucumber-report")
@CucumberOptions(features ="src/test/resources/features/", glue="cucumber.definitions", plugin = { "html:target/cucumber-report",
        "json:target/cucumber-report/cucumber.json", "pretty:target/cucumber-pretty.txt",
        "usage:target/cucumber-report/cucumber-usage.json", "junit:target/cucumber-report/cucumber-results.xml" })

public class TestRunner {

}
