package cucumber.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginEsikaPage extends PageFactory {

	private WebDriverWait wait;
	
	@AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
	protected static AndroidElement MSG_PERMITIR;
	
	@AndroidFindBy(id = "btn_login")
	protected static AndroidElement BTN_INICIAR_SESION;
	
	@AndroidFindBy(id = "ivw_select")
	protected static AndroidElement CBO_PAIS;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ListView/android.widget.RelativeLayout[7]/android.widget.TextView")
	protected static AndroidElement LIST_PAIS;
	
	@AndroidFindBy(id = "biz.belcorp.consultoras.esika:id/tie_username")
	protected static AndroidElement TXT_USUARIO;
	
	@AndroidFindBy(id = "biz.belcorp.consultoras.esika:id/tie_password") 
	protected static  AndroidElement TXT_PASSWORD;
	
	@AndroidFindBy(id = "btn_login") 
	protected static  AndroidElement BTN_LOGIN;
	
	public LoginEsikaPage(WebDriver driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		wait = new WebDriverWait(driver, 1000);
	}
	
	public void ClickPermitir() {
		wait.until(ExpectedConditions.elementToBeClickable(MSG_PERMITIR));
		MSG_PERMITIR.click();
	}
	
	public void ClickIniciarSesion() {
		wait.until(ExpectedConditions.elementToBeClickable(BTN_INICIAR_SESION));
		BTN_INICIAR_SESION.click();
	}
	
	public void ClickListaPais() {
		wait.until(ExpectedConditions.elementToBeClickable(CBO_PAIS));
		CBO_PAIS.click();
	}
	
	public void ClickPais() {
		wait.until(ExpectedConditions.elementToBeClickable(LIST_PAIS));
		LIST_PAIS.click();
	}
	
	public void EscribirUsuario(String usuario) {
		wait.until(ExpectedConditions.visibilityOf(TXT_USUARIO));
		TXT_USUARIO.setValue(usuario);
	}
	
	public void EscribirPassword(String password) {
		wait.until(ExpectedConditions.visibilityOf(TXT_PASSWORD));
		TXT_PASSWORD.setValue(password);
	}
	
	public void ClickLogin() throws InterruptedException {
		wait.until(ExpectedConditions.elementToBeClickable(BTN_LOGIN));
		BTN_LOGIN.click();
		Thread.sleep(10000);
	}
	
}
