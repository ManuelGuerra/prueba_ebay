package cucumber.definicion;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.pageobjects.LoginEsikaPage;;

public class LoginEsikaDefinicion {

	LoginEsikaPage LoginEsikaPage;
	
	public LoginEsikaDefinicion() {
		LoginEsikaPage=new LoginEsikaPage(Hooks.driver);
	}
	
	@Given("^Permitir$")
	public void Click_Permitir() {
		LoginEsikaPage.ClickPermitir();
	}
	
	@And("^Click en el boton Iniciar Sesion$")
	public void click_Iniciar_sesion() {
		LoginEsikaPage.ClickIniciarSesion();	
	}
	
	@And("^Click en Lista de Pais$")
	public void click_lista_pais() {
		LoginEsikaPage.ClickListaPais();
	}
	
	@And("^Click en Pais$")
	public void click_pais() {
		LoginEsikaPage.ClickPais();
	}
	
	@And("^Escribir el usuario ([^\"]*)$")
	public void escribir_usuario(String usuario) {
		LoginEsikaPage.EscribirUsuario(usuario);
	}
	
	@And("^Escribir el password ([^\"]*)$")
	public void escribir_password(String password) {
		LoginEsikaPage.EscribirPassword(password);
	}
	
	@And("^Click en el boton Login$")
	public void click_Login() throws InterruptedException {
		LoginEsikaPage.ClickLogin();
	}

	
	
}
