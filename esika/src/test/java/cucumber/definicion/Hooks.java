package cucumber.definicion;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.android.AndroidDriver;

public class Hooks {

	public static AndroidDriver<WebElement> driver;

	@Before
    public static void setUp() throws MalformedURLException{
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
//		capabilities.setCapability("deviceName", "Xperia XA1 Ultra");
		capabilities.setCapability("deviceName", "HUAWEI Mate 9 lite");
		capabilities.setCapability("platformName", "Android");
//		capabilities.setCapability("platformVersion", "8.0");
		capabilities.setCapability("platformVersion", "7.0");
		capabilities.setCapability("appPackage","biz.belcorp.consultoras.esika");
//		capabilities.setCapability("appPackage","biz.belcorp.consultoras.esika.dev");
		capabilities.setCapability("appActivity","biz.belcorp.consultoras.feature.splash.SplashActivity");
//		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "chorme");
//		capabilities.setCapability(MobileCapabilityType.APP, "C:\\Users\\alexander.avila\\Desktop\\apk\\interestcalculator.apk");
		driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
    }

    @After
    public static void tearDown(){
        driver.quit();
    }
}
